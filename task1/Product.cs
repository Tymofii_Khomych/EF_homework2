﻿using System.ComponentModel.DataAnnotations;

namespace task1
{
    public class Product
    {
        public Guid ProductID { get; private set; }
        public Guid ProductAlterID { get; private set; }
        [Required]
        public string Name { get; private set; }
        public double Cost { get; private set; }
        [StringLength(50, MinimumLength = 5)]
        public string Description { get; private set; }
        public int Quantity { get; private set; }

        public Product(string name, double cost, string description, int quantity)
        {
            ProductID = Guid.NewGuid();
            Name = name;
            Cost = cost;
            Description = description;
            Quantity = quantity;
        }
    }
}

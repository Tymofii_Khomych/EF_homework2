﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    public class Error
    {
        public string Message { get; set; }
        public DateTime Time;
        public string Request { get; set; }
        public StatusCode StatusCode { get; set; }
    }

    public enum StatusCode
    {
        Ok = 200,
        NotFound = 404,
        ServerError = 500
    }
}
